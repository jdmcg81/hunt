class Hunt extends React.Component {

	constructor(props) {
		super(props);
		
		this.state = {
			timerRemaining: props.timerLength,
			collectedAssbutts: 0,
			spawns: [],
			gameRunning: false,
			collectedNotAssbutts: 0
		};
	}
	
	componentDidMount() {
	}
	
	componentWillUnmount() {
		clearInterval(this.timerID);
	}

	startGame = () => {
		this.spawnedThings = 0;
		this.spawnLength = 1000;
		this.assbuttBonus = 1000;
		this.levelingValue = 30;
		this.timerID = setInterval(
			() => this.tick(),
			1000
		);
		this.spawnSomeThings()
		this.setState({
			collectedAssbutts: 0,
			timerRemaining: this.props.timerLength,
			gameRunning: true,
			collectedAssbutt: false,
			collectedNotAssbutt: false
		});
	}

	randomize = (min, max) => {
		return Math.floor(Math.random() * (+max - +min)) + +min;
	}

	spawnSomeThings = () => {
		let spawns = [];
		for (i=0; i<=this.randomize(0,4); i++) {

			spawns.push(this.spawnSomething());
		}
		this.spawnID = setInterval(
			() => this.despawnSomething(),
			this.spawnLength
		);
		this.setState({
			spawns: spawns
		});
	}

	spawnSomething = () => {
		let isAssbutt = this.randomize(0,3) > 0;
		this.spawnedThings++;
		if (this.spawnedThings < 300 && this.spawnedThings % 15 === 0) {
			this.spawnLength -= this.levelingValue;
			this.assbuttBonus += this.levelingValue;
		}

		let spawnLocation = this.setSpawnLocation(this.randomize(0, 100), this.randomize(0, 100));
		return {
			spawnImage: this.setSpawnImage(isAssbutt),
			spawnClass: isAssbutt ? "assbutt" : "notAssbutt",
			spawnLocation: spawnLocation
		};
	}

	despawnSomething = () => {
		clearInterval(this.spawnID);
		this.spawnSomeThings();
	}
	
	clickedAThing = event => {
		if (event.target.classList.contains("assbutt")) {
			this.collectAssbutt();
		} else {
			this.collectNotAssbutt();
		}
	}

	collectAssbutt = () => {
		let newTimerRemaining = Math.round(this.state.timerRemaining + (this.assbuttBonus / 1000));
		this.collectionChange = setInterval(
			() => this.cancelCollected(),
			200
		);
		this.setState({
			collectedAssbutts: this.state.collectedAssbutts + 1,
			timerRemaining: (newTimerRemaining > this.props.timerLength) ? this.props.timerLength : newTimerRemaining,
			collectedAssbutt: true
		});
	}

	collectNotAssbutt = () => {
		let newTimerRemaining = Math.round(this.state.timerRemaining - (this.assbuttBonus / 1000));
		this.collectionChange = setInterval(
			() => this.cancelCollected(),
			200
		);
		this.setState({
			timerRemaining: (newTimerRemaining < 0) ? 0 : newTimerRemaining,
			collectedNotAssbutt: true
		});
	}

	cancelCollected = () => {
		clearInterval(this.collectionChange);
		this.setState({
			collectedNotAssbutt: false,
			collectedAssbutt: false
		});
	}

	missedItByThatMuch() {

	}

	endGame() {

	}

	tick() {
		let newTimerRemaining = this.state.timerRemaining - 1;
		if (newTimerRemaining <= 0) {
			this.timerExpired();
		}
		this.setState({
			timerRemaining: newTimerRemaining
		});
	}

	timerExpired() {
		clearInterval(this.timerID);
		clearInterval(this.spawnID);
		this.setState({
			gameRunning:false
		});
	}

	setSpawnImage = spawnIsAssbutt => {
		return spawnIsAssbutt ? "images/assbutt.png" : "images/notAssbutt.png"
	}

	setSpawnLocation = (xLocation, yLocation) => {
		let xSide = "left";
		let ySide = "top";

		if (xLocation > 50) {
			xSide = "right";
			xLocation = 100 - xLocation;
		}
		if (yLocation > 50) {
			ySide = "bottom";
			yLocation = 100 - yLocation;
		}
		return {
			[xSide]: xLocation + "%",
			[ySide]: yLocation + "%"
		};
	}

	updateSpawns = () => {
		return this.state.spawns.map((val, index) => {
			return <div key={index} className="assbutt-container" style={val.spawnLocation}>
				<img className={"clickable " + val.spawnClass} src={val.spawnImage} onTouchStart={this.clickedAThing} />
			</div>
		});
	}

	rulesText = () => {
		return <div><h2>Hey Assbutt, Some Rules!</h2>
		Tap on the Assbutt to collect it. For each Assbutt you successfully collect, you'll get some bonus time. But be careful! If you collect a Not-Assbutt, you'll lose extra time.
		<p>
			<button className="play-button" onClick={this.startGame}>Play!</button>
		</p></div>
	}

	gameOverText = () => {
		return <div><h2>Hey Assbutt!</h2>
		What a good little ant you are. You found {this.state.collectedAssbutts} Assbutts!
		<p>
			<button className="play-button" onClick={this.startGame}>Play again?</button>
		</p></div>
	}
	
	render() {
		return <div>
			<div className={"cover-modal " + (this.state.gameRunning ? "hide" : "")}>
				{(this.state.collectedAssbutts > 0 ? this.gameOverText() : this.rulesText())}
			</div>
			<div className={"hunt-container " + (this.state.gameRunning ? "" : "faded")}>
				<div className="title-bar">
					Get Those Assbutts!
				</div>
				<div className="skor-bar">
					<div className="timer-bar">
						<div className="timer-numbers">
							{this.state.timerRemaining}
						</div>
						<div className="timer" style={{
							width: (this.state.timerRemaining / this.props.timerLength * 100) + "%"
						}}></div>
					</div>
					<div className="yer-skor">Assbutts: {this.state.collectedAssbutts}</div>
				</div>
				<div className={"hunt-grid " + (this.state.collectedAssbutt ? "good-bg " : "") + (this.state.collectedNotAssbutt ? "bad-bg " : "")}>
					{this.updateSpawns()}
				</div>
			</div>
		</div>;
	}
}
ReactDOM.render(<Hunt timerLength={60} />, document.getElementById("hunt"));