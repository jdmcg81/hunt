/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./public/js/jsx/apps/hunt.jsx");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./public/js/jsx/apps/hunt.jsx":
/*!*************************************!*\
  !*** ./public/js/jsx/apps/hunt.jsx ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nvar Hunt =\n/*#__PURE__*/\nfunction (_React$Component) {\n  _inherits(Hunt, _React$Component);\n\n  function Hunt(props) {\n    var _this;\n\n    _classCallCheck(this, Hunt);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(Hunt).call(this, props));\n\n    _this.startGame = function () {\n      _this.spawnedThings = 0;\n      _this.spawnLength = 1000;\n      _this.assbuttBonus = 1000;\n      _this.levelingValue = 30;\n      _this.timerID = setInterval(function () {\n        return _this.tick();\n      }, 1000);\n\n      _this.spawnSomeThings();\n\n      _this.setState({\n        collectedAssbutts: 0,\n        timerRemaining: _this.props.timerLength,\n        gameRunning: true,\n        collectedAssbutt: false,\n        collectedNotAssbutt: false\n      });\n    };\n\n    _this.randomize = function (min, max) {\n      return Math.floor(Math.random() * (+max - +min)) + +min;\n    };\n\n    _this.spawnSomeThings = function () {\n      var spawns = [];\n\n      for (i = 0; i <= _this.randomize(0, 4); i++) {\n        spawns.push(_this.spawnSomething());\n      }\n\n      _this.spawnID = setInterval(function () {\n        return _this.despawnSomething();\n      }, _this.spawnLength);\n\n      _this.setState({\n        spawns: spawns\n      });\n    };\n\n    _this.spawnSomething = function () {\n      var isAssbutt = _this.randomize(0, 3) > 0;\n      _this.spawnedThings++;\n\n      if (_this.spawnedThings < 300 && _this.spawnedThings % 15 === 0) {\n        _this.spawnLength -= _this.levelingValue;\n        _this.assbuttBonus += _this.levelingValue;\n      }\n\n      var spawnLocation = _this.setSpawnLocation(_this.randomize(0, 100), _this.randomize(0, 100));\n\n      return {\n        spawnImage: _this.setSpawnImage(isAssbutt),\n        spawnClass: isAssbutt ? \"assbutt\" : \"notAssbutt\",\n        spawnLocation: spawnLocation\n      };\n    };\n\n    _this.despawnSomething = function () {\n      clearInterval(_this.spawnID);\n\n      _this.spawnSomeThings();\n    };\n\n    _this.clickedAThing = function (event) {\n      if (event.target.classList.contains(\"assbutt\")) {\n        _this.collectAssbutt();\n      } else {\n        _this.collectNotAssbutt();\n      }\n    };\n\n    _this.collectAssbutt = function () {\n      var newTimerRemaining = Math.round(_this.state.timerRemaining + _this.assbuttBonus / 1000);\n      _this.collectionChange = setInterval(function () {\n        return _this.cancelCollected();\n      }, 200);\n\n      _this.setState({\n        collectedAssbutts: _this.state.collectedAssbutts + 1,\n        timerRemaining: newTimerRemaining > _this.props.timerLength ? _this.props.timerLength : newTimerRemaining,\n        collectedAssbutt: true\n      });\n    };\n\n    _this.collectNotAssbutt = function () {\n      var newTimerRemaining = Math.round(_this.state.timerRemaining - _this.assbuttBonus / 1000);\n      _this.collectionChange = setInterval(function () {\n        return _this.cancelCollected();\n      }, 200);\n\n      _this.setState({\n        timerRemaining: newTimerRemaining < 0 ? 0 : newTimerRemaining,\n        collectedNotAssbutt: true\n      });\n    };\n\n    _this.cancelCollected = function () {\n      clearInterval(_this.collectionChange);\n\n      _this.setState({\n        collectedNotAssbutt: false,\n        collectedAssbutt: false\n      });\n    };\n\n    _this.setSpawnImage = function (spawnIsAssbutt) {\n      return spawnIsAssbutt ? \"images/assbutt.png\" : \"images/notAssbutt.png\";\n    };\n\n    _this.setSpawnLocation = function (xLocation, yLocation) {\n      var _ref;\n\n      var xSide = \"left\";\n      var ySide = \"top\";\n\n      if (xLocation > 50) {\n        xSide = \"right\";\n        xLocation = 100 - xLocation;\n      }\n\n      if (yLocation > 50) {\n        ySide = \"bottom\";\n        yLocation = 100 - yLocation;\n      }\n\n      return _ref = {}, _defineProperty(_ref, xSide, xLocation + \"%\"), _defineProperty(_ref, ySide, yLocation + \"%\"), _ref;\n    };\n\n    _this.updateSpawns = function () {\n      return _this.state.spawns.map(function (val, index) {\n        return React.createElement(\"div\", {\n          key: index,\n          className: \"assbutt-container\",\n          style: val.spawnLocation\n        }, React.createElement(\"img\", {\n          className: \"clickable \" + val.spawnClass,\n          src: val.spawnImage,\n          onTouchStart: _this.clickedAThing\n        }));\n      });\n    };\n\n    _this.rulesText = function () {\n      return React.createElement(\"div\", null, React.createElement(\"h2\", null, \"Hey Assbutt, Some Rules!\"), \"Tap on the Assbutt to collect it. For each Assbutt you successfully collect, you'll get some bonus time. But be careful! If you collect a Not-Assbutt, you'll lose extra time.\", React.createElement(\"p\", null, React.createElement(\"button\", {\n        className: \"play-button\",\n        onClick: _this.startGame\n      }, \"Play!\")));\n    };\n\n    _this.gameOverText = function () {\n      return React.createElement(\"div\", null, React.createElement(\"h2\", null, \"Hey Assbutt!\"), \"What a good little ant you are. You found \", _this.state.collectedAssbutts, \" Assbutts!\", React.createElement(\"p\", null, React.createElement(\"button\", {\n        className: \"play-button\",\n        onClick: _this.startGame\n      }, \"Play again?\")));\n    };\n\n    _this.state = {\n      timerRemaining: props.timerLength,\n      collectedAssbutts: 0,\n      spawns: [],\n      gameRunning: false,\n      collectedNotAssbutts: 0\n    };\n    return _this;\n  }\n\n  _createClass(Hunt, [{\n    key: \"componentDidMount\",\n    value: function componentDidMount() {}\n  }, {\n    key: \"componentWillUnmount\",\n    value: function componentWillUnmount() {\n      clearInterval(this.timerID);\n    }\n  }, {\n    key: \"missedItByThatMuch\",\n    value: function missedItByThatMuch() {}\n  }, {\n    key: \"endGame\",\n    value: function endGame() {}\n  }, {\n    key: \"tick\",\n    value: function tick() {\n      var newTimerRemaining = this.state.timerRemaining - 1;\n\n      if (newTimerRemaining <= 0) {\n        this.timerExpired();\n      }\n\n      this.setState({\n        timerRemaining: newTimerRemaining\n      });\n    }\n  }, {\n    key: \"timerExpired\",\n    value: function timerExpired() {\n      clearInterval(this.timerID);\n      clearInterval(this.spawnID);\n      this.setState({\n        gameRunning: false\n      });\n    }\n  }, {\n    key: \"render\",\n    value: function render() {\n      return React.createElement(\"div\", null, React.createElement(\"div\", {\n        className: \"cover-modal \" + (this.state.gameRunning ? \"hide\" : \"\")\n      }, this.state.collectedAssbutts > 0 ? this.gameOverText() : this.rulesText()), React.createElement(\"div\", {\n        className: \"hunt-container \" + (this.state.gameRunning ? \"\" : \"faded\")\n      }, React.createElement(\"div\", {\n        className: \"title-bar\"\n      }, \"Get Those Assbutts!\"), React.createElement(\"div\", {\n        className: \"skor-bar\"\n      }, React.createElement(\"div\", {\n        className: \"timer-bar\"\n      }, React.createElement(\"div\", {\n        className: \"timer-numbers\"\n      }, this.state.timerRemaining), React.createElement(\"div\", {\n        className: \"timer\",\n        style: {\n          width: this.state.timerRemaining / this.props.timerLength * 100 + \"%\"\n        }\n      })), React.createElement(\"div\", {\n        className: \"yer-skor\"\n      }, \"Assbutts: \", this.state.collectedAssbutts)), React.createElement(\"div\", {\n        className: \"hunt-grid \" + (this.state.collectedAssbutt ? \"good-bg \" : \"\") + (this.state.collectedNotAssbutt ? \"bad-bg \" : \"\")\n      }, this.updateSpawns())));\n    }\n  }]);\n\n  return Hunt;\n}(React.Component);\n\nReactDOM.render(React.createElement(Hunt, {\n  timerLength: 60\n}), document.getElementById(\"hunt\"));\n\n//# sourceURL=webpack:///./public/js/jsx/apps/hunt.jsx?");

/***/ })

/******/ });