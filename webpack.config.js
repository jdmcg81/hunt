const glob = require("glob");
const path = require("path");
const webpack = require("webpack");

const entryArray = glob.sync("./public/js/jsx/apps/**/*.jsx");

const entryObject = entryArray.reduce((acc, item) => {
    let res = item.replace(' ', ''); //strip any spaces
    res = item.replace(/\.jsx/, ''); //get rid of the jsx path
    const arr = res.split('/');
    const match = res.match(/\/js\/jsx\/(.*)/);

    acc[match[1]] = item;
    return acc;
}, {});


module.exports = {
    entry: entryObject,
    mode: "development",
    watch: true,
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
                options: { presets: ["@babel/preset-env", "@babel/preset-react"] }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    resolve: { extensions: ["*", ".js", ".jsx"] },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, "public/js/react-compiled/")
    }
};